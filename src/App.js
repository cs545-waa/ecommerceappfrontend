import Login from "./containers/login/Login";
import Header from "./containers/commons/Header"
import Auth from "./api/Auth";
import useToken from "./hooks/useToken";

const App = () => {
    const {token, setToken} = useToken();
    const auth = new Auth(token, setToken);

  return (
    <>
      <Header />
      <main className="App">
        <Login auth={auth} />
      </main>
    </>
  );
};

export default App;
