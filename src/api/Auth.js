import Config from "./Config";
import axios from "axios";

class Auth {
    token;
    setToken;

    constructor(argToken, argSetToken) {
        this.token = argToken;
        this.setToken = argSetToken;
        this.config = new Config();
    }

    async loginUser(credentials) {
        return fetch(this.config.LOGIN_URL, {
            method: "POST",
            mode: "cors",
            headers: {
                ...this.config.defaultHeaders(),
            },
            body: JSON.stringify(credentials),
        })
            .then((response) => Promise.all([response, response.json()]))
            .then(([response, json]) => {
                if (!response.ok) {
                    return { success: false, error: json };
                }
                this.storeTokens(json);
                return { success: true, data: json };
            })
            .catch((e) => {
                return this.handleError(e);
            });
        // return axios.get("http://localhost:8000/login", credentials).then(res => {
        //     console.log(res.data);//get all reviews from backend
        //     // under data there will be reviews
        //
        // })
    }

    handleResponseError(response) {
        this.clearTokens();
        if (!response.ok) {
            const error = response.json();
            console.log(error);
            throw Error(error);
        }
        return response;
    }

    handleError(error) {
        this.clearTokens();
        const err = new Map([
            [TypeError, "Can't connect to server."],
            [SyntaxError, "There was a problem parsing the response."],
            [Error, error.message],
        ]).get(error.constructor);
        console.log(err);
        return err;
    }

    storeTokens(json) {
        this.setToken(json);
        this.config.storeAccessToken(json.token);
    }

    clearTokens() {
        this.config.storeAccessToken("");
        this.setToken(null);
    }
}

export default Auth;