class Config {

    LOGIN_URL = "http://localhost:8000/login";

    ACCESS_TOKEN = "accessToken";


    defaultHeaders() {
        return {
            "Content-Type": "application/json",
            Accept: "application/json",
        };
    }

    headersWithAuthorization() {
        return {
            ...this.defaultHeaders(),
            Authorization: localStorage.getItem(this.ACCESS_TOKEN),
        };
    }

    storeAccessToken(token) {
        localStorage.setItem(this.ACCESS_TOKEN, `Bearer ${token}`);
    }
}

export default Config;