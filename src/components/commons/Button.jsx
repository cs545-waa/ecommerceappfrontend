const Button = ({text}) => {
  return (
    <a className="btn btn-info" href="#">{text}</a>
  )
}

export default Button