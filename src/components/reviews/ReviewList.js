import React from 'react';
import List from "../commons/List";

const ReviewList = ({reviews}) => {

    reviews = [{"product": {"name": "galaxy"}, "comment": "very bad", "rating": 3.4}];
    return (
        <div>
            {
                reviews.map(review => {

                   return <List value={review}/>

                }
                )
            }
        </div>
    );
};

export default ReviewList;