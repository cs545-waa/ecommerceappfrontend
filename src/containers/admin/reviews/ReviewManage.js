import React, {useEffect, useState} from 'react';
import ReviewList from "../../../components/reviews/ReviewList";
import Button from "../../../components/commons/Button";
import axios from "axios";
import useToken from "../../../hooks/useToken";

const ReviewManage = () => {

   const {token, setToken} = useToken();
    const [reviews, setReviews] = useState('');
    //
    useEffect(() => {

        if (token !== '') {
            axios.get("http://localhost:8000/reviews", {headers: {"Authorization": `Bearer ${token}`}}).then(res => {
                console.log("revie", res.data);//get all reviews from backend
                setReviews(res.data); // under data there will be reviews

            })
        }


    }, []);
    return (
        <>
            {/*<ReviewList reviews={reviews} />*/}
            <Button text="Approve"/>
        </>
    );
};

export default ReviewManage;