import { useState } from "react";
import Button from "../../components/commons/Button";

const Header = () => {
  const [isLoggedin, setIsLoggedin] = useState(true);
  const [loginBtn, setBtn] = useState(<Button text="Login"/>)

  const items = ["home", "aboout", "contact", "products"]

  return (
    <header>
      <nav className="d-flex p-3 border-bottom border-2 border-dark shadow">
        <div className="col-4">
          <a className="btn btn-info" href="#">eCommerce</a>
        </div>
        <div className="col-8 d-flex gap-2">
          {
            items.map(item => <Button text= {item} />)
          }
          {
            isLoggedin && loginBtn
          }
        </div>
      </nav>
    </header>
  );
};

export default Header;
