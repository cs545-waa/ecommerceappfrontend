import {useState} from "react";
import useToken from "../../hooks/useToken";

const Login = ({auth}) => {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [errMsg, setErrMsg] = useState();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const res = await auth.loginUser(({
            "email": email,
            "password": password
        }));

        if (res && res.success) {
            setErrMsg(null);
        } else {
            setErrMsg(
                res && typeof res === "string" ? res : "Invalid Username/Password"
            );
        }

    };
    return (
        <div className='container border border-3 px-5 mt-5'>
            <h2>Login </h2>

            <form onSubmit={handleSubmit}>
                <div>

                    <label htmlFor="username">
                        Username
                    </label>
                    <input
                        id="username"
                        name="username"
                        type="username"
                        autoComplete="username"
                        required
                        placeholder="Username"
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>
                <div>
                    <label htmlFor="password" className="sr-only">
                        Password
                    </label>
                    <input
                        id="password"
                        name="password"
                        type="password"
                        required
                        placeholder="Password"
                        onChange={(e) => setPassword(e.target.value)}/>
                </div>
                <div>
                    <button type="submit">Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login;